/// <reference path="point.ts" />

class Actor {
    position: Point;
    mass: number;
    speed: Point;
    acceleration: Point;
    radius: number;
    forces: Point;

    constructor(position, mass, rad) {
        this.position = position;
        this.mass = mass;
        this.speed = new Point(0, 0);
        this.acceleration = new Point(0, 0);
        this.forces = new Point(0, 0);
        this.radius = rad;
        this.forces = new Point(0, 0);
    }

    Update(time: number, canvas): void {
        if (this.position.x + this.radius >= canvas.width) {
            this.position.x = canvas.width - this.radius;
            this.speed.x *= -1;
        } else if (this.position.x - this.radius < 0) {
            this.position.x = this.radius;
            this.speed.x *= -1;
        }
        if (this.position.y + this.radius >= canvas.height) {
            this.position.y = canvas.height - this.radius;
            this.speed.y *= -1;
        } else if (this.position.y - this.radius < 0) {
            this.position.y = this.radius;
            this.speed.y *= -1;
        }
        this.acceleration = this.forces.TimesS(1/this.mass);
        this.speed = this.speed.AddP(this.acceleration.TimesS(time));
        this.speed = this.speed.TimesS(0.995);
        this.position = this.position.AddP(this.speed.TimesS(time));
        this.forces = new Point(0, 0);
    }

    Collide(other) {
        var diff = this.position.SubP(other.position);
        var distance = diff.Length() - this.radius - other.radius;
        if (distance < 0) {
            this.position = this.position.AddS(distance / 2);
            other.position = other.position.AddS(-distance / 2);
            this.forces = this.forces.AddP(diff.TimesS(0.4));
            other.forces = other.forces.AddP(diff.TimesS(-0.4));
        }
    }
}
