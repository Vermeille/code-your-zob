/// <reference path="actor.ts" />
/// <reference path="shoots.ts" />
/// <reference path="drawable.ts" />

function min(m: number, n: number): number {
    return m < n ? m : n;
}

var sheet = new Image();

function Background(ctx, totalTime: number): number {
    ctx.drawImage(sheet, 1600 - totalTime % 800, 0);
    ctx.drawImage(sheet, 800 - totalTime % 800, 0);
    ctx.drawImage(sheet, - totalTime % 800, 0);
}

class Enemy {
    drawable: Drawable;

    constructor() {
        this.drawable = new Drawable(1200, 400, "resources/supinfo.png");
    }

    Draw(ctx): void {
        this.drawable.Draw(ctx);
    }

    Update(time: number): void {

    }
}

window.onload = () => {
    var canvas = <HTMLCanvasElement>document.getElementById('canv');
    var ctx = canvas.getContext('2d');

    window.requestAnimationFrame(MainLoop);
    var john = new Drawable(400, 300, "resources/guitar.png");
    var shoots = new Shoots((s) => s.actor.position.x += 5);
    var vilain_shoots = new Shoots((s) => { 
            s.actor.forces = john.actor.position.SubP(s.actor.position)
                .Normalize().TimesS(5);
            s.Update(1, canvas);
        });
    var totalTime = 0;
    var vilain = new Enemy();
    sheet.src = "resources/sheet.png";

    $('#canv').keydown((ev) => {
            if (ev.keyCode == 38)
                john.actor.speed.y -= 4;
            if (ev.keyCode == 39)
                john.actor.speed.x += 4;
            if (ev.keyCode == 37)
                john.actor.speed.x -= 4;
            if (ev.keyCode == 40)
                john.actor.speed.y += 4;
            if (ev.keyCode == 32) {
                shoots.Push(john.actor.position.AddP(new Point(12, 15)));
                john.actor.forces.x -= 40;
            }
        });

    function MainLoop(): void {
        totalTime += 2;
        ctx.fillStyle = "#8866CC";
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        Background(ctx, totalTime);
        john.Draw(ctx);
        john.Update(1, canvas);
        shoots.Draw(ctx);
        shoots.Update(1);
        vilain.Draw(ctx);
        vilain.Update(1);
        vilain_shoots.Draw(ctx);
        vilain_shoots.Update(1);
        if (totalTime % 20 == 0)
            vilain_shoots.Push(vilain.drawable.actor.position);
        window.requestAnimationFrame(MainLoop);
    }
};
