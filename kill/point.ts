class Point {
    x: number;
    y: number;

    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    Length(): number {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }

    LengthSquare(): number {
        return this.x * this.x + this.y + this.y;
    }

    TimesS(s: number): Point {
        return new Point(this.x * s, this.y * s);
    }

    AddS(s: number): Point {
        return new Point(this.x + s, this.y + s);
    }

    AddP(p: Point): Point {
        return new Point(this.x + p.x, this.y + p.y);
    }

    Neg(): Point {
        return new Point(-this.x, -this.y);
    }

    SubP(p: Point): Point {
        return this.AddP(p.Neg());
    }

    Normalize()
    {
        var len = this.Length();
        return new Point(this.x / len, this.y / len);
    }
}

