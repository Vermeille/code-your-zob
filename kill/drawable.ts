class Drawable {
    actor: Actor;
    img: Image;

    constructor(x: number, y: number, src: string) {
        this.img = new Image();
        this.img.src = src;
        this.actor = new Actor(new Point(x, y), 100,
                min(this.img.width, this.img.height) / 2);
    }

    Draw(ctx): void {
        ctx.drawImage(this.img, this.actor.position.x - this.actor.radius,
                this.actor.position.y - this.actor.radius);
    }

    Update(time: number, canvas: HTMLCanvasElement) {
        this.actor.Update(time, canvas);
    }
}

