
class Shoots {
    shoots: Drawable[];
    updatefun: (Drawable) => void;

    constructor(updatefun) {
        this.shoots = [];
        this.updatefun = updatefun;
    }

    Push(p: Point): void {
        this.shoots.push(new Drawable(p.x, p.y, "resources/fireball.png"));
    }

    Draw(ctx): void {
        for (var i = 0; i < this.shoots.length; ++i)
            this.shoots[i].Draw(ctx);
    }

    Update(time: number): void {
        for (var i = 0; i < this.shoots.length; ++i)
            this.updatefun(this.shoots[i]);
    }
}

