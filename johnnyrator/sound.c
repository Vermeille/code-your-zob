#include <math.h>
#include <stdio.h>
#include <stdlib.h>

const float TAU = 2 * M_PI;

float hash(float t) { return fmodf(sin(t * 12362.13715) * 782371.7283234, 1.); }
float noise(float t) {
    int ti = t;
    float td = t - ti;
    return td * hash(ti) + (1 - td) * hash(ti + 1);
}

float sinw(float t, float f) { return sinf(t * TAU * f); }

float sqw(float t, float f) {
    float phase = fmodf(t / (1 / f), 1.) < 0.5;
    return 2 * phase - 1;
}

float triw(float t, float f) {
    float x = fmodf(t / (1 / f), 1.);
    if (x < 0.5) {
        return x * 4 - 1;
    } else {
        return 1 - (x - 0.5) * 4;
    }
}

float instr(float t, float f) {
    return sin(TAU * t * f + sin(TAU * t * f + M_PI / 1));
}

float saww(float t, float f) {
    float x = fmodf(t / (1 / f), 1.);
    return -x * 2 + 1;
}

float whitew(float t) { return noise(t * 40000); }

float freq(int n) { return powf(2, (n - 49) / 12.) * 440; }

int i(float f) { return f; }

float lead(float t) {
    int notes[] = {49, 52, 56, 61};
    return instr(t, freq(notes[i(t * 8) % 4] - 12 * (i(t) % 2)));
}

float bass(float t) {
    int notes[] = {25, 25, 26, 28};
    int n = notes[i(t * 2) % 4];
    return 0.33 * (sqw(t, freq(n)) + sqw(t, freq(n) * 1.002) +
                   sqw(t, freq(n) * 0.998));
}

float lowpass(float prev, float cur, float c) {
    return c * prev + (1 - c) * cur;
}

float lead2(float t) {
    int notes[] = {35, -1};
    int notes2[] = {42, -1};
    int n = notes[i(t * 6) % 2];
    if (n == -1) {
        return 0;
    }
    return instr(
        t,
        freq(n));  // triw(t, freq(n)) + triw(t, freq(notes2[i(t * 6) % 2]));
}

// play with ./sound | aplay -f FLOAT_LE -r 44100 -c 1
int main() {
    float t = 0;
    for (int i = 0; i < 44100 * 3; ++i) {
        t += 1 / 44100.;
        float sample = 0;
        sample += 0.2 * lead(t);
        // sample += 0.5 * bass(t);
        fwrite(&sample, sizeof(sample), 1, stdout);
    }
    return 0;
}
