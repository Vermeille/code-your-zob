#include <iostream>
#include <string>
#include <map>
#include <sstream>
#include <deque>
#include <vector>

#include <cstdlib>
#include <ctime>

template <class T>
void dump(const T& dek)
{
    for (const auto& w : dek)
    {
        std::cout << w << " ";
    }
}

void dump(const std::map<std::deque<std::string>, std::vector<std::string>>& m)
{
    for (const auto& p : m)
    {
        std::cout << "[";
        dump(p.first);
        std::cout << "] => ";
        dump(p.second);
        std::cout << std::endl;
    }
}

void Learn(std::map<std::deque<std::string>, std::vector<std::string>>& markov,
        int nb_markov)
{
    std::string line;

    std::getline(std::cin, line);
    std::istringstream iss(line);
    std::string current;
    std::deque<std::string> ws;

    for (int i = 0; i < nb_markov; ++i)
    {
        iss >> current;
        ws.push_back(current);
    }

    do
    {
        while (iss.good())
        {
            iss >> current;
            if (!iss.good())
                current += "\n";
            markov[ws].push_back(current);
            ws.pop_front();
            ws.push_back(current);
        }
        std::getline(std::cin, line);
        iss.clear();
        iss.str(line);
    } while (std::cin.good());
}

void Sing(std::map<std::deque<std::string>, std::vector<std::string>>& markov,
        int nb_markov)
{
    std::deque<std::string> ws;
    std::string current;

    srand(time(nullptr));

    auto iter = markov.begin();

    std::advance(iter, rand() % markov.size());

    ws = iter->first;

    for (int i = 0; i < nb_markov; ++i)
    {
        std::cout << ws[i] << " ";
    }

    for (size_t i = 0; i < 256; ++i)
    {
        if (markov[ws].size())
        {
            current = markov[ws][rand() % markov[ws].size()];
            std::cout << current << " ";
            ws.pop_front();
            ws.push_back(current);
        }
    }
}

int main(int argc, char const *argv[])
{
    std::map<std::deque<std::string>, std::vector<std::string>> markov;

    int nb = std::atoi(argv[1]);
    Learn(markov, nb);
    dump(markov);
    Sing(markov, nb);

    return (0);
}

