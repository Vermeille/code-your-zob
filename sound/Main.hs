#! /usr/bin/env runhugs +l
--
-- Main.hs
-- Copyright (C) 2013 vermeille <guillaume.sanchez@epita.fr>
--
-- Distributed under terms of the MIT license.
--

module Main where

import TotalWaveforms
import Data.ByteString.Lazy as BL (putStr)
import Data.Int
import System.Random
import Freqs
import Text.Printf
import Data.Binary.Put as Bin

addW :: Int16 -> Int16 -> Int16
addW x y = (x `div` 2 + y `div` 2)

mulW :: Int16 -> Int16 -> Int16
mulW x y = truncate $ (fromIntegral x / 32768.0) * (fromIntegral y / 32768.0) * 32768.0

play :: Int -> (Float -> Int -> [Int16]) -> Float -> [Int16]
play ms fun freq =
            (Prelude.take nbData $ fun freq 0)
            where nbData = msToDatas ms

msToDatas :: Int -> Int
msToDatas ms = truncate $ 44100.0 / (1000.0 / fromIntegral ms)

genSound :: Float -> StdGen -> Int -> [Int16]
genSound freq rnd i =
          ((play 200 sinW freq) ++
          play 50 squareW freq)
          -- take (msToDatas 10) $ whiteW rnd `thenP`
          ++ (zipWith addW (play 80 triW freq) (play 80 squareW (freq * 2)))

genSong :: [Int] -> IO [Int16]
genSong notes = do
        g <- newStdGen
        return $ concatMap (\n -> genSound (noteToFreqs (n)) g 0) notes

main = do sound <- genSong notes
          let whole = cycle sound
          BL.putStr . Bin.runPut . mapM_ (Bin.putWord16le . fromIntegral) $ whole
          -- BL.putStr . Bin.runPut . mapM_ (Bin.int16BE) $ whole
          {-
          putStrLn "var values = "
          print $ concat $ sound
          putStrLn ";"
          -- -}

notes = [48, 55, 56, 48, 51, 55, 50, 53,
         48, 55, 56, 48, 51, 55, 50, 53,
         47, 50, 51, 55, 51, 50, 51, 48,
         47, 50, 51, 55, 51, 50, 51, 48]

