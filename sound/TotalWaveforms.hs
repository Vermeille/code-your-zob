#! /usr/bin/env runhugs +l
--
-- TotaWaveforms.hs
-- Copyright (C) 2013 vermeille <guillaume.sanchez@epita.fr>
--
-- Distributed under terms of the MIT license.
--

module TotalWaveforms (
            sinW,
            squareW,
            sawW,
            triW,
            whiteW
            ) where

import Data.Word
import Data.Int
import System.Random

getPeriod freq = freq * 2 * pi / 44100

{---------------------------------------------------------------------------
-                                  sinW                                   -
---------------------------------------------------------------------------}


sinW :: Float -> Int -> [Int16]
sinW freq i = sinW' i (getPeriod freq)

sinW' :: Int -> Float -> [Int16]
sinW' i period =
        let new = (round $ (* 32767.0) $ sin (fromIntegral i * period)) in
              new:new:sinW' (i + 1) period

{---------------------------------------------------------------------------
-                                 squareW                                 -
---------------------------------------------------------------------------}

squareW :: Float -> Int -> [Int16]
squareW freq i = squareW' i (getPeriod freq)

squareW' :: Int -> Float -> [Int16]
squareW' i period = let new = (round . (* 32767.0) . fromIntegral . sign
                          $ sin (fromIntegral i * period)) in
            new:new:squareW' (i + 1) period

sign x | x < 0 = -1
       | otherwise = 1

{---------------------------------------------------------------------------
-                                  sawW                                   -
---------------------------------------------------------------------------}

sawW i = sawW' i

sawW' :: Int -> Float -> [Int16]
sawW' i period = sawW'' i period (-32767)

sawW'' :: Int -> Float -> Int -> [Int16]
sawW'' i period tempSample
        | i < samplesPerWavelength period =
                let new = fromIntegral tempSample in
                new:new:sawW'' (i + 1) period (tempSample + ampStep period)
        | otherwise = (-32767):(-32767):sawW'' 0 period (-32767 + ampStep period)
        where
            samplesPerWavelength :: Float -> Int
            samplesPerWavelength freq = truncate $ 44100.0 / period
            ampStep :: Float -> Int
            ampStep period = (44100 * 2) `div` samplesPerWavelength period

{---------------------------------------------------------------------------
-                                  triW                                   -
---------------------------------------------------------------------------}

triW :: Float -> Int-> [Int16]
triW period i = triW' period (-32766)
                   $ (44100 * 3)
                       `div` (samplesPerWavelength period)
       where
           samplesPerWavelength :: Float -> Int
           samplesPerWavelength freq = truncate $ 44100.0 / freq

triW' :: Float -> Int -> Int -> [Int16]
triW' period tempSample ampStep
       | abs tempSample > 32767 =
               let new = fromIntegral $ tempSample + ampStep in
                   new:new:triW' period (tempSample - ampStep) (-ampStep)
       | otherwise =
           fromIntegral tempSample
           :fromIntegral tempSample:triW' period (tempSample + ampStep) ampStep

{---------------------------------------------------------------------------
-                                 whiteW                                  -
---------------------------------------------------------------------------}

whiteW :: StdGen -> [Int16]
whiteW rnd = randoms rnd

