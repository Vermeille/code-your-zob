/**
** @file total_waveforms.h for sound in sound
*/

#ifndef TOTAL_WAVEFORMS_H_
# define TOTAL_WAVEFORMS_H_

void Sinus(double freq, int ms);
void Square(double freq, int ms);
void Saw(double freq, int ms);
void Triangle(double freq, int ms);
void White(int ms);

#endif /* !TOTAL_WAVEFORMS_H_ */

