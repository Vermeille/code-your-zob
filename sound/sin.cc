#include <cmath>
#include <iostream>
#include <cstdlib>
#include <ctime>

#include "total_waveforms.h"

int main()
{
    double freqs[] = {
#define X(A) A,
# include "freqs.def"
#undef X
    };

    srand(time(nullptr));

    //  49 51    54 56 58
    // 48 50 52 53 55 57 59 60
    int note = 0;
    int song[] = {
        48, 55, 56, 48, 51, 55, 50, 53,
        48, 55, 56, 48, 51, 55, 50, 53,
        47, 50, 51, 55, 51, 50, 51, 48,
        47, 50, 51, 55, 51, 50, 51, 48
    };
    while (true)
    {
        for (size_t i = 0; i < 5; ++i)
        {
            Square(freqs[song[note]], 20);
            Sinus(freqs[song[note]-12], 15);
            Sinus(freqs[song[note]], 15);
            Triangle(freqs[song[note]], 5);
            Saw(freqs[song[note]], 30);
            Saw(freqs[song[note]+12], 30);
        }
        note = (note + 1) % (sizeof (song) / sizeof (song[0]));
    }
    return (0);
}

