#include <cmath>
#include <iostream>
#include <cstdlib>

void Sinus(double freq, int ms)
{
    unsigned nb_data = 44100 / (1000 / ms);
    double period = freq * 2 * M_PI / 44100;

    for (unsigned i = 0; i < nb_data; ++i)
    {
        short res = 32768 * sin(i * period);
        std::cout.write(reinterpret_cast<char*>(&res), sizeof (short));
        std::cout.write(reinterpret_cast<char*>(&res), sizeof (short));
    }
}

double sign(double x)
{
    return (x <= 0) ? -1 : 1;
}

void Square(double freq, int ms)
{
    unsigned nb_data = 44100 / (1000 / ms);
    double period = freq * 2 * M_PI / 44100;

    for (unsigned i = 0; i < nb_data; ++i)
    {
        short res = 32767 * sign(sin(i * period));
        std::cout.write(reinterpret_cast<char*>(&res), sizeof (short));
        std::cout.write(reinterpret_cast<char*>(&res), sizeof (short));
    }
}

void Saw(double freq, int ms)
{
    unsigned nb_data = 44100 / (1000 / ms);
    int samplesPerWavelength = 44100 / freq;
    short ampStep = ((44100 * 2) / samplesPerWavelength);
    short tempSample = (short)-32767;
    unsigned totalSamplesWritten = 0;
    while (totalSamplesWritten < nb_data)
    {
        tempSample = (short)-32767;
        for (int i = 0; i < samplesPerWavelength
                && totalSamplesWritten < nb_data; i++)
        {
            tempSample += ampStep;
            std::cout.write(reinterpret_cast<char*>(&tempSample),
                    sizeof (short));
            std::cout.write(reinterpret_cast<char*>(&tempSample),
                    sizeof (short));
            totalSamplesWritten++;
            totalSamplesWritten++;
        }
    }
}

void Triangle(double freq, int ms)
{
    int nb_data = 44100 / (1000 / ms);
    int samplesPerWavelength = 44100 / freq;
    short ampStep = ((44100 * 3) / samplesPerWavelength);
    short tempSample = (short)-32767;
    for (int i = 0; i < nb_data; i++)
    {
        if (abs(tempSample) >= 32767)
            ampStep = (short)-ampStep;

        tempSample += ampStep;
        std::cout.write(reinterpret_cast<char*>(&tempSample),
                sizeof (short));
        std::cout.write(reinterpret_cast<char*>(&tempSample),
                sizeof (short));
    }
}

void White(int ms)
{
    unsigned nb_data = 44100 / (1000 / ms);

    for (size_t i = 0; i < nb_data; ++i)
    {
        short res = (rand() % 32767) * 2 - 32767;
        std::cout.write(reinterpret_cast<char*>(&res),
                sizeof (short));
        std::cout.write(reinterpret_cast<char*>(&res),
                sizeof (short));
    }
}

